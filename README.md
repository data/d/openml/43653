# OpenML dataset: NBA-PLAYERS--2016-2019

https://www.openml.org/d/43653

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This Dataset was created for an University project in Milan. The goal of the project was to create a Robust Model to predict the All Star Game score for each player. 
The score is calculated by dividing the players by conference and position held in the field (external or internal),
the athletes are ranked in descending order depending on the number of votes taken for each category
voter, so as to obtain three different rankings. From these for each player the rank is calculated (1st
Position involves Rank = 1, 2nd Position Rank = 2 etc. . . ). With the values obtained then an average is calculated
weighed, with weight 0.50 to that of the votes of the fans and 0.25 to the two remaining.
Doing this work we have merged some datasets from kaggle (https://www.kaggle.com/noahgift/social-power-nba),  basketball-reference.com  and hoopshype.com.
Obviously for our work we didn't use all of the variables and for a problem of indipendent observations we took only the last seasons' observation for each player.
Other analysis could be performed using salary as a target but also a cluster analysis for players or a PCA.
We wouldn't be here without the help of others. Thank you Riccardo,Alfredo and Daniel.
Some variables:
POS1= Main position (some players have a second position called POS2)
G= Games played
GS= Games started
MP= Minutes played
FG =Field Goals Per Game
FGA=Field Goal Attempts Per Game
FG.= Field Goal Percentage
X3P= 3-Point Field Goals Per Game
X3PA= 3-Point Field Goal Attempts Per Game
X3P.= FG on 3-Pt FGAs.
X2P =2-Point Field Goals Per Game
X2PA =2-Point Field Goal Attempts Per Game
X2P.= FG on 2-Pt FGAs.
eFG. = Effective Field Goal Percentage
FT=Free Throws Per Game
FTA = Free Throw Attempts Per Game
FT.= Free Throw Percentage
ORB = Offensive Rebounds Per Game
DRB = Defensive Rebounds Per Game
TRB = Total Rebounds Per Game
AST = Assists Per Game
STL= Steals Per Game
BLK = Blocks Per Game
TOV = Turnovers Per Game
PF = Personal Fouls Per Game
PTS =Points Per Game
MEAN_VIEWS= Daily views on wikipedia
PLAY= If the player played in the all star game

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43653) of an [OpenML dataset](https://www.openml.org/d/43653). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43653/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43653/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43653/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

